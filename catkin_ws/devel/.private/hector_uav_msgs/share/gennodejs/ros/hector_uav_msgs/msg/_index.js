
"use strict";

let HeadingCommand = require('./HeadingCommand.js');
let Compass = require('./Compass.js');
let YawrateCommand = require('./YawrateCommand.js');
let MotorPWM = require('./MotorPWM.js');
let ServoCommand = require('./ServoCommand.js');
let ThrustCommand = require('./ThrustCommand.js');
let VelocityZCommand = require('./VelocityZCommand.js');
let MotorStatus = require('./MotorStatus.js');
let Altimeter = require('./Altimeter.js');
let HeightCommand = require('./HeightCommand.js');
let RawImu = require('./RawImu.js');
let MotorCommand = require('./MotorCommand.js');
let VelocityXYCommand = require('./VelocityXYCommand.js');
let ControllerState = require('./ControllerState.js');
let RawRC = require('./RawRC.js');
let RC = require('./RC.js');
let Supply = require('./Supply.js');
let RuddersCommand = require('./RuddersCommand.js');
let AttitudeCommand = require('./AttitudeCommand.js');
let PositionXYCommand = require('./PositionXYCommand.js');
let RawMagnetic = require('./RawMagnetic.js');
let TakeoffActionFeedback = require('./TakeoffActionFeedback.js');
let PoseActionFeedback = require('./PoseActionFeedback.js');
let LandingFeedback = require('./LandingFeedback.js');
let LandingGoal = require('./LandingGoal.js');
let TakeoffActionGoal = require('./TakeoffActionGoal.js');
let LandingActionGoal = require('./LandingActionGoal.js');
let PoseAction = require('./PoseAction.js');
let LandingActionResult = require('./LandingActionResult.js');
let TakeoffGoal = require('./TakeoffGoal.js');
let PoseResult = require('./PoseResult.js');
let PoseGoal = require('./PoseGoal.js');
let LandingActionFeedback = require('./LandingActionFeedback.js');
let TakeoffActionResult = require('./TakeoffActionResult.js');
let LandingResult = require('./LandingResult.js');
let TakeoffAction = require('./TakeoffAction.js');
let PoseActionResult = require('./PoseActionResult.js');
let PoseFeedback = require('./PoseFeedback.js');
let TakeoffFeedback = require('./TakeoffFeedback.js');
let TakeoffResult = require('./TakeoffResult.js');
let LandingAction = require('./LandingAction.js');
let PoseActionGoal = require('./PoseActionGoal.js');

module.exports = {
  HeadingCommand: HeadingCommand,
  Compass: Compass,
  YawrateCommand: YawrateCommand,
  MotorPWM: MotorPWM,
  ServoCommand: ServoCommand,
  ThrustCommand: ThrustCommand,
  VelocityZCommand: VelocityZCommand,
  MotorStatus: MotorStatus,
  Altimeter: Altimeter,
  HeightCommand: HeightCommand,
  RawImu: RawImu,
  MotorCommand: MotorCommand,
  VelocityXYCommand: VelocityXYCommand,
  ControllerState: ControllerState,
  RawRC: RawRC,
  RC: RC,
  Supply: Supply,
  RuddersCommand: RuddersCommand,
  AttitudeCommand: AttitudeCommand,
  PositionXYCommand: PositionXYCommand,
  RawMagnetic: RawMagnetic,
  TakeoffActionFeedback: TakeoffActionFeedback,
  PoseActionFeedback: PoseActionFeedback,
  LandingFeedback: LandingFeedback,
  LandingGoal: LandingGoal,
  TakeoffActionGoal: TakeoffActionGoal,
  LandingActionGoal: LandingActionGoal,
  PoseAction: PoseAction,
  LandingActionResult: LandingActionResult,
  TakeoffGoal: TakeoffGoal,
  PoseResult: PoseResult,
  PoseGoal: PoseGoal,
  LandingActionFeedback: LandingActionFeedback,
  TakeoffActionResult: TakeoffActionResult,
  LandingResult: LandingResult,
  TakeoffAction: TakeoffAction,
  PoseActionResult: PoseActionResult,
  PoseFeedback: PoseFeedback,
  TakeoffFeedback: TakeoffFeedback,
  TakeoffResult: TakeoffResult,
  LandingAction: LandingAction,
  PoseActionGoal: PoseActionGoal,
};
