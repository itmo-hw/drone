#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/catkin_ws/devel/.private/drone_scene:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/catkin_ws/devel/.private/drone_scene/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/catkin_ws/devel/.private/drone_scene/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/catkin_ws/build/drone_scene'
export ROSLISP_PACKAGE_DIRECTORIES="/catkin_ws/devel/.private/drone_scene/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/catkin_ws/src/drone_scene:$ROS_PACKAGE_PATH"