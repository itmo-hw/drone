import rospy
from hector_uav_msgs.srv import EnableMotors
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

cmd_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

k = 10
i = 8
z_des = 2
def callback(msg):
    z = msg.pose.pose.position.z
    dotz = msg.twist.twist.linear.z
    uz = k * (z_des - z) - i * dotz
    cmd_msg = Twist()
    cmd_msg.linear.z = uz
    cmd_pub.publish(cmd_msg)


def image_bot_callback(msg):
    try:
        cv_image_b = bridge.imgmsg_to_cv2(msg, "bgr8")
    except CvBridgeError as e:
        print(e)
    cv2.imshow("Bottom", cv_image_b)
    cv2.waitKey(3)


def enable_motors():
    rospy.wait_for_service('/enable_motors')
    try:
        call_em = rospy.ServiceProxy('/enable_motors', EnableMotors)
        resp1 = call_em(True)
        return resp1.success
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


def main():
    rospy.init_node('takeoff_node')
    enable_motors()
    rospy.Subscriber('/ground_truth/state', Odometry, callback)
    rospy.Subscriber('/cam_1/camera/image', Image, image_bot_callback)
    rospy.spin()


if __name__ == '__main__':
    main()