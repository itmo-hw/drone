import time
import numpy as np
import rospy
from geometry_msgs.msg import Point, Twist
from sensor_msgs.msg import Image
from hector_uav_msgs.srv import EnableMotors
import cv2
from cv_bridge import CvBridge, CvBridgeError
from nav_msgs.msg import Odometry


V_MAX = 1.5

default_alt = 2.8
kp_z = 5
kd_z = 3

kp_y =  0.007
kd_y =  0.001

kp_w =  0.025
kd_w =  0.001

RING_AVOIDANCE_TIME = 3
ring_fl_thr_time = 3


class Trace_control():

    def __init__(self):
        rospy.init_node('line_follower', anonymous=True)

        self.enable_motors(True)

        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.cv_bridge = CvBridge()

        rospy.on_shutdown(self.shutdown)

        rospy.Subscriber("cam_1/camera/image", Image, self.camera_bot_callback)
        rospy.Subscriber('/ground_truth/state', Odometry, self.state_callback)
        rospy.Subscriber('/cam_2/camera/image', Image, self.camera_front_callback)

        self.position = Point()
        self.twist = Twist()
        self.omega_error = 0
        self.omega_error_prev = 0
        self.y_error = 0
        self.y_error_prev = 0
        self.camera_bot = []
        self.camera_front = []
        self.state = "free_flight"
        self.red_ring_detected = False
        self.blue_ring_detected = False
        self.z_des = default_alt
        self.time_start_up = 0
        self.avoidance_time = 0
        self.e_x_blue, self.e_y_blue = 0, 0
        self.time_start_fl_thr = 0
        self.fl_thr_time = 0

        rospy.on_shutdown(self.shutdown)

        self.rate = rospy.Rate(30)


    def enable_motors(self, state):
        try:
            rospy.wait_for_service('enable_motors', 2)
            call_service = rospy.ServiceProxy('enable_motors', EnableMotors)
            response = call_service(state)
        except Exception as e:
            print("Error while try to enable motors: ", e)


    def state_callback(self, msg):
        self.position = msg.pose.pose.position
        self.twist = msg.twist.twist  


    def camera_bot_callback(self, msg):
        """ Computer vision stuff"""
        try:
            self.camera_bot = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))

        # Filtr
        grey_image = cv2.cvtColor(self.camera_bot, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(grey_image, 8, 255, cv2.THRESH_BINARY_INV)
        self.camera_bot = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

        cv2.line(self.camera_bot, (160, 0), (160, 240), (0, 123, 0), 1)
        cv2.line(self.camera_bot, (0, 120), (320, 120), (0, 123, 0), 1)

        # "steering" conrol
        top_points = np.where(mask[10] >= 10)
        mid_points = np.where(mask[msg.height / 2] >= 10)
        if  (not np.isnan(np.average(top_points)) and not np.isnan(np.average(mid_points))):
            top_line_point = int(np.average(top_points))
            mid_line_point = int(np.average(mid_points))
            self.omega_error = top_line_point - mid_line_point
            
            cv2.circle(self.camera_bot, (top_line_point, 10), 5, (0,0,255), 1)
            cv2.circle(self.camera_bot, (mid_line_point, int(msg.height/2)), 5, (0,0,255), 1)
            cv2.line(self.camera_bot, (mid_line_point, int(msg.height/2)), (top_line_point, 10), (0, 0, 255), 3)

        # y-offset control
        __, cy_list = np.where(mask >= 10)
        if not np.isnan(np.average(cy_list)):
            cy = int(np.average(cy_list))
            self.y_error = msg.width / 2 - cy
            
            cv2.circle(self.camera_bot, (cy, int(msg.height/2)), 7, (0,255,0), 1)
            cv2.line(self.camera_bot, (160, 120), (cy, int(msg.height/2)), (0, 255, 0), 3)
    

    def camera_front_callback(self, msg):
        try:
            self.camera_front = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            print(e)
        # red
        lower = np.uint8([0, 0, 90])
        upper = np.uint8([30, 30, 120])
        self.camera_front, red_pose, red_radius  = self.ring_detector(self.camera_front, lower, upper, (0,0,255))

        # blue
        lower = np.uint8([40, 20, 20])
        upper = np.uint8([80, 50, 50])
        self.camera_front, blue_pose, blue_radius = self.ring_detector(self.camera_front, lower, upper, (255,0,0))

        # print(red_radius, blue_radius)

        if 50 < red_radius < 70 or 50 < blue_radius < 80:
            if red_radius > blue_radius:
                self.blue_ring_detected = False
                self.red_ring_detected = True
            else:
                self.red_ring_detected = False
                self.blue_ring_detected = True
                
                # offset in ring xy-plane to fly through center of a ring
                # error = <center of image> - <center of ring>
                self.e_x_blue = 160 - blue_pose[0]
                self.e_y_blue = 120 - blue_pose[1]
        else:
            self.blue_ring_detected = False
            self.red_ring_detected = False
        # save results
        self.image_2 = self.camera_front


    def ring_detector(self, image, lower, upper, color):
        color_mask = cv2.inRange(image, lower, upper)
        color_contours, _ = cv2.findContours(color_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        if color_contours:
            max_len_c = 0
            c = color_contours[0]
            for i in range(0, len(color_contours)):
                if len(color_contours[i]) > max_len_c:
                    c = color_contours[i]
                    max_len_c = len(color_contours[i])
            self.color_distance = max_len_c
            M = cv2.moments(c)
            if M['m00'] != 0:
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])
            else:
                cx = 0
                cy = 0
            (x1,y1), color_r = cv2.minEnclosingCircle(c)
            if color_r > 10:
                image = cv2.circle(image, (cx, cy), radius=5, color=color, thickness=-1)
                cv2.drawContours(color_r, c, -1, (0,255,0), 1)
                color_r = cv2.circle(color_r, (int(x1), int(y1)), radius=int(color_r), color=color, thickness=4)       
                return image, (x1,y1), color_r[0]
        return image, (0,0), 0


    def show_image(self, img, title):
        cv2.imshow(title, img)
        cv2.waitKey(3)


    def fsm_update(self):
        if self.red_ring_detected:
            print('avoid')
            self.state = "drone_up"
        elif 0 < self.avoidance_time <= RING_AVOIDANCE_TIME:
            self.state = "drone_up"
            print('avoid v2')           
        elif self.blue_ring_detected:
            self.state = "drone_blue_ring"
            print('pass through')
        elif 0 < self.fl_thr_time <= ring_fl_thr_time:
            self.state = "drone_blue_ring_free"
            print('pass through v2')
        else:
            self.state = "free_flight"
            print('free')
            self.time_start_up = 0
            self.avoidance_time = 0
            self.time_start_fl_thr = 0
            self.fl_thr_time = 0
                

    def spin(self):       
        # Initialisations

        time_start = rospy.get_time()
        time_prev = time_start
        while not rospy.is_shutdown():
            try:
                # Time stuff
                t = rospy.get_time() - time_start
                dt = t - time_prev
                time_prev = t
                if dt == 0:
                    dt = 1 / 30.

                # ring avoid
                self.fsm_update()
                if self.state == "drone_up":
                    self.z_des = 5
                    if self.time_start_up == 0:
                        self.time_start_up = rospy.get_time()
                    self.avoidance_time = rospy.get_time() - self.time_start_up
                elif self.state == "drone_blue_ring":
                    self.z_des += 0.001 * self.e_y_blue
                    if self.time_start_fl_thr == 0:
                        self.time_start_fl_thr = rospy.get_time()
                    self.fl_thr_time = rospy.get_time() - self.time_start_fl_thr
                elif self.state == 'drone_blue_ring_free':
                    self.fl_thr_time = rospy.get_time() - self.time_start_fl_thr
                elif self.state == "free_flight":
                    self.z_des = default_alt
                else:
                    rospy.logerr("Error: state name error!")

                print(self.avoidance_time)
                print(self.fl_thr_time)

                #altitude controller
                u_z = kp_z * (self.z_des - self.position.z) - kd_z * self.twist.linear.z

                #Steering control
                u_omega_z = kp_w * self.omega_error - kd_w * (self.omega_error - self.omega_error_prev) / dt
                self.omega_error_prev = self.omega_error

                #Offset control
                u_y = kp_y * self.y_error - kd_y * (self.y_error - self.y_error_prev) / dt
                self.y_error_prev = self.y_error

                # Pub
                twist_msg = Twist()
                twist_msg.linear.x = V_MAX
                twist_msg.linear.y = u_y
                twist_msg.linear.z = u_z
                twist_msg.angular.z = -u_omega_z
                self.cmd_vel_pub.publish(twist_msg)

                if len(self.camera_bot) > 0 and len(self.camera_front) > 0:
                    self.show_image(self.camera_bot, title='Bottom')
                    self.show_image(self.camera_front, title='Front')

            except KeyboardInterrupt:
                break
        
            self.rate.sleep()

    def shutdown(self):
        self.enable_motors(False)


def main():
    ctrl = Trace_control()
    ctrl.spin()


if __name__=="__main__":
    main()