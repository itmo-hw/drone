import time
import numpy as np
import rospy
from geometry_msgs.msg import Point, Twist
from sensor_msgs.msg import Image
from hector_uav_msgs.srv import EnableMotors
import cv2
from cv_bridge import CvBridge, CvBridgeError
from nav_msgs.msg import Odometry


V_MAX = 2.0

z_des = 2.6
kp_z = 5
kd_z = 3

kp_y =  0.008
kd_y =  0.002

kp_w =  0.045
kd_w =  0.0015


class SimpleMover():

    def __init__(self):
        rospy.init_node('line_follower', anonymous=True)

        self.enable_motors(True)

        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.cv_bridge = CvBridge()

        rospy.on_shutdown(self.shutdown)

        rospy.Subscriber("cam_1/camera/image", Image, self.camera_callback)
        rospy.Subscriber('/ground_truth/state', Odometry, self.state_callback)

        self.position = Point()
        self.twist = Twist()
        self.omega_error = 0
        self.omega_error_prev = 0
        self.y_error = 0
        self.y_error_prev = 0

        rospy.on_shutdown(self.shutdown)

        self.rate = rospy.Rate(30)


    def enable_motors(self, state):
        try:
            rospy.wait_for_service('enable_motors', 2)
            call_service = rospy.ServiceProxy('enable_motors', EnableMotors)
            response = call_service(state)
        except Exception as e:
            print("Error while try to enable motors: ", e)

    def state_callback(self, msg):
        self.position = msg.pose.pose.position
        self.twist = msg.twist.twist  


    def camera_callback(self, msg):
        """ Computer vision stuff"""
        try:
            cv_image = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))

        # Filtr
        grey_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(grey_image, 8, 255, cv2.THRESH_BINARY_INV)
        cv_image = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

        cv2.line(cv_image, (160, 0), (160, 240), (0, 123, 0), 1)
        cv2.line(cv_image, (0, 120), (320, 120), (0, 123, 0), 1)

        # "steering" conrol
        top_points = np.where(mask[10] >= 10)
        mid_points = np.where(mask[msg.height / 2] >= 10)
        if  (not np.isnan(np.average(top_points)) and not np.isnan(np.average(mid_points))):
            top_line_point = int(np.average(top_points))
            mid_line_point = int(np.average(mid_points))
            self.omega_error = top_line_point - mid_line_point
            
            cv2.circle(cv_image, (top_line_point, 10), 5, (0,0,255), 1)
            cv2.circle(cv_image, (mid_line_point, int(msg.height/2)), 5, (0,0,255), 1)
            cv2.line(cv_image, (mid_line_point, int(msg.height/2)), (top_line_point, 10), (0, 0, 255), 3)

        # y-offset control
        __, cy_list = np.where(mask >= 10)
        if not np.isnan(np.average(cy_list)):
            cy = int(np.average(cy_list))
            self.y_error = msg.width / 2 - cy
            
            cv2.circle(cv_image, (cy, int(msg.height/2)), 7, (0,255,0), 1)
            cv2.line(cv_image, (160, 120), (cy, int(msg.height/2)), (0, 255, 0), 3)

        self.show_image(cv_image, title='Camera 1')


    def show_image(self, img, title):
        cv2.imshow(title, img)
        cv2.waitKey(3)


    def spin(self):       
        # Initialisations

        time_start = rospy.get_time()
        time_prev = time_start
        while not rospy.is_shutdown():
            try:
                # Time stuff
                t = rospy.get_time() - time_start
                dt = t - time_prev
                time_prev = t
                if dt == 0:
                    dt = 1 / 30.

                #altitude controller
                u_z = kp_z * (z_des - self.position.z) - kd_z * self.twist.linear.z

                #Steering control
                u_omega_z = kp_w * self.omega_error - kd_w * (self.omega_error - self.omega_error_prev) / dt
                self.omega_error_prev = self.omega_error

                #Offset control
                u_y = kp_y * self.y_error - kd_y * (self.y_error - self.y_error_prev) / dt
                self.y_error_prev = self.y_error


                twist_msg = Twist()
                twist_msg.linear.x = V_MAX
                twist_msg.linear.y = u_y
                twist_msg.linear.z = u_z
                twist_msg.angular.z = -u_omega_z
                self.cmd_vel_pub.publish(twist_msg)

            except KeyboardInterrupt:
                break

            self.rate.sleep()

    def shutdown(self):
        self.enable_motors(False)


def main():
    ctrl = SimpleMover()
    ctrl.spin()


if __name__=="__main__":
    main()